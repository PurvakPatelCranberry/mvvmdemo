package com.example.mvvmdemo.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.mvvmdemo.BR

class AddPlace : BaseObservable() {
    @Bindable
    var placeName: String? = null

    @Bindable
    var placeDescription: String? = null

    @Bindable
    var placeTags: String? = null

    @Bindable
    var placeNote: String? = null

    @Bindable
    var isMakePlacePublic = false

    @Bindable
    var toastMessage: String? = null
        set(toastMessage) {
            field = toastMessage
            notifyPropertyChanged(BR.toastMessage)
        }

}