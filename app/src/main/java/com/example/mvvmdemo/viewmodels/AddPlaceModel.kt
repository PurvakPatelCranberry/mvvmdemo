package com.example.mvvmdemo.viewmodels

import android.text.TextUtils
import androidx.databinding.BaseObservable
import com.example.mvvmdemo.interfaces.AddPlaceUiHelper
import com.example.mvvmdemo.model.AddPlace
import com.example.mvvmdemo.views.AddPlaceActivity

class AddPlaceModel(private val addPlaceActivity: AddPlaceActivity, private val addPlaceUiHelper: AddPlaceUiHelper, private val addPlace: AddPlace) : BaseObservable() {
    fun onNameNextClicked() {
        if (!TextUtils.isEmpty(addPlace.placeName)) {
            addPlaceUiHelper.onAddNameStatus(true)
        } else {
            val errorMessage = "Enter valid Name"
            addPlace.toastMessage = errorMessage
            addPlaceUiHelper.onAddNameStatus(false)
        }
    }

    fun onDescriptionNextClicked() {
        if (!TextUtils.isEmpty(addPlace.placeDescription)) {
            addPlaceUiHelper.onAddDescriptionStatus(true)
        } else {
            val errorMessage = "Enter valid Description"
            addPlace.toastMessage = errorMessage
            addPlaceUiHelper.onAddDescriptionStatus(false)
        }
    }

    fun onTagsNextClicked() {
        if (!TextUtils.isEmpty(addPlace.placeTags)) {
            addPlaceUiHelper.onAddTagsStatus(true)
        } else {
            val errorMessage = "Enter valid Tags"
            addPlace.toastMessage = errorMessage
            addPlaceUiHelper.onAddTagsStatus(false)
        }
    }

    fun onSaveClicked() {
        if (!TextUtils.isEmpty(addPlace.placeNote)) {
            addPlaceUiHelper.onAddNoteStatus(true)
        } else {
            val errorMessage = "Enter valid Note"
            addPlace.toastMessage = errorMessage
            addPlaceUiHelper.onAddNoteStatus(false)
        }
    }

    fun onNameBackArrowClicked() {
        addPlaceActivity.finish()
    }

    fun onDescriptionBackArrowClicked() {
        addPlaceUiHelper.onDescriptionBackArrowClicked()
    }

    fun onTagsBackArrowClicked() {
        addPlaceUiHelper.onTagsBackArrowClicked()
    }

    fun onNoteBackArrowClicked() {
        addPlaceUiHelper.onNoteBackArrowClicked()
    }

    fun onNameCloseClicked() {
        addPlaceActivity.finish()
    }

    fun onDescriptionCloseClicked() {
        addPlaceUiHelper.onDescriptionBackArrowClicked()
    }

    fun onTagsCloseClicked() {
        addPlaceUiHelper.onTagsBackArrowClicked()
    }

    fun onNoteCloseClicked() {
        addPlaceUiHelper.onNoteBackArrowClicked()
    }

    fun onBackClicked() {
        addPlaceActivity.finish()
    }

    fun onCloseClicked() {
        addPlaceActivity.finish()
    }

}