package com.example.mvvmdemo.interfaces

interface AddPlaceUiHelper {
    fun onAddNameStatus(status: Boolean)
    fun onAddDescriptionStatus(status: Boolean)
    fun onAddTagsStatus(status: Boolean)
    fun onAddNoteStatus(status: Boolean)

    //    void onNameBackArrowClicked();
    fun onDescriptionBackArrowClicked()
    fun onTagsBackArrowClicked()
    fun onNoteBackArrowClicked()
    fun onShowProgress(status: Boolean) //    void closeAll();
    //    void onNameCloseClicked(); // Name - Handle individual close Button
    //    void onDescriptionCloseClicked(); // Description - Handle individual close Button
    //    void onTagsCloseClicked(); // Tags - Handle individual close Button
    //    void onNoteCloseClicked(); // Note - Handle individual close Button
}