package com.example.mvvmdemo.views

import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import android.widget.RadioButton
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import com.example.mvvmdemo.R
import com.example.mvvmdemo.databinding.ActivityAddPlaceBinding
import com.example.mvvmdemo.interfaces.AddPlaceUiHelper
import com.example.mvvmdemo.model.AddPlace
import com.example.mvvmdemo.viewmodels.AddPlaceModel
import com.google.android.material.tabs.TabLayout

class AddPlaceActivity : AppCompatActivity(), AddPlaceUiHelper {
    private var addPlaceModel: AddPlaceModel? = null
    private var addPlace: AddPlace? = null
    private var relativeLayoutName: RelativeLayout? = null
    private var relativeLayoutDescription: RelativeLayout? = null
    private var relativeLayoutTags: RelativeLayout? = null
    private var relativeLayoutNote: RelativeLayout? = null
    private var isRadioButtonMakePlacePublic = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityAddPlaceBinding: ActivityAddPlaceBinding = DataBindingUtil.setContentView(
                this, R.layout.activity_add_place)

        initObject()
        activityAddPlaceBinding.setAddPlaceModel(addPlace)
        activityAddPlaceBinding.setAddPlaceViewModel(addPlaceModel)
        activityAddPlaceBinding.executePendingBindings()
    }

    private fun initObject() {
        addPlace = AddPlace()
        addPlaceModel = AddPlaceModel(this@AddPlaceActivity, this, addPlace!!)
        relativeLayoutName = findViewById(R.id.relativeLayoutName)
        relativeLayoutDescription = findViewById(R.id.relativeLayoutDescription)
        relativeLayoutTags = findViewById(R.id.relativeLayoutTags)
        relativeLayoutNote = findViewById(R.id.relativeLayoutNote)
        val radioButtonMakePlacePublic = findViewById<RadioButton>(R.id.radioButtonMakePlacePublic)
        radioButtonMakePlacePublic.setOnClickListener(View.OnClickListener { // Check which radiobutton was pressed
            isRadioButtonMakePlacePublic = !isRadioButtonMakePlacePublic
            radioButtonMakePlacePublic.setChecked(isRadioButtonMakePlacePublic)
        })
        radioButtonMakePlacePublic.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked -> addPlace!!.isMakePlacePublic = isChecked })
    }

    override fun onAddNameStatus(status: Boolean) {
        println("Name Status = $status")
        if (status) {
            relativeLayoutName!!.visibility = View.GONE
            relativeLayoutDescription!!.visibility = View.VISIBLE
        } else {
        }
    }

    override fun onAddDescriptionStatus(status: Boolean) {
        println("Description Status = $status")
        if (status) {
            relativeLayoutDescription!!.visibility = View.GONE
            relativeLayoutTags!!.visibility = View.VISIBLE
        } else {
        }
    }

    override fun onAddTagsStatus(status: Boolean) {
        println("Tags Status = $status")
        if (status) {
            relativeLayoutTags!!.visibility = View.GONE
            relativeLayoutNote!!.visibility = View.VISIBLE
        } else {
        }
    }

    override fun onAddNoteStatus(status: Boolean) {
        println("Note Status = $status")
        if (status) {
            println("Name = " + addPlace!!.placeName)
            println("Description = " + addPlace!!.placeDescription)
            println("Tags = " + addPlace!!.placeTags)
            println("Note = " + addPlace!!.placeNote)
            println("Note = " + addPlace!!.isMakePlacePublic)
        } else {
        }
    }

    override fun onShowProgress(status: Boolean) {}
    override fun onDescriptionBackArrowClicked() {
        relativeLayoutDescription!!.visibility = View.GONE
        relativeLayoutName!!.visibility = View.VISIBLE
    }

    override fun onTagsBackArrowClicked() {
        relativeLayoutTags!!.visibility = View.GONE
        relativeLayoutDescription!!.visibility = View.VISIBLE
    }

    override fun onNoteBackArrowClicked() {
        relativeLayoutNote!!.visibility = View.GONE
        relativeLayoutTags!!.visibility = View.VISIBLE
    }

    companion object {
        @JvmStatic
        @BindingAdapter("toastMessage")
        fun runMe(view: View, message: String?) {
            if (message != null) Toast.makeText(view.context, message, Toast.LENGTH_SHORT).show()
        }
    }
}